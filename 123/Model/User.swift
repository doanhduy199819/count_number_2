//
//  User.swift
//  123
//
//  Created by Anh Duy on 4/1/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import Foundation

class User {
    var name : String?
    var uid : String?
    var score : Int?
    
    init(name: String, uid: String, score: Int) {
        self.name = name
        self.uid = uid
        self.score = score
    }
    init() {
        
    }
}
