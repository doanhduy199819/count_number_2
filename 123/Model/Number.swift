//
//  Array.swift
//  123
//
//  Created by Anh Duy on 3/16/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import Foundation
import Firebase

class Number {
    
    var first = 1
    var last = 16
    var count = 1
    
    var currentScore: Int = 0
    var bestScore: Int = 0
    
    var numberArray : [Int] = []
    var numberArraySpare : [Int] = []
    var removeArray : [Int] = []
        
    private init() {
        configureNumberArray()
    }
    func resetNumberArray() {
        numberArray.removeAll()
        numberArraySpare.removeAll()
        first = 1
        last = 16
        count = 1
        
        for i in first...last {
            self.numberArray.append(i)
        }
        self.numberArray = self.numberArray.shuffled()
        self.numberArraySpare = self.numberArray
        print("Reset numberArray")
        
    }
    
    func configureNumberArray() {
        for i in first...last {
            self.numberArray.append(i)
        }
        self.numberArray = self.numberArray.shuffled()
        self.numberArraySpare = self.numberArray
    }
    
    func changeNumberArray() {
        if numberArray.count == 5 {
            first += 16
            last += 16
            numberArraySpare.removeAll()
            for i in first...last {
                numberArraySpare.append(i)
            }
            numberArraySpare = numberArraySpare.shuffled()
            print(numberArraySpare)
        }
        if numberArray.count == 0 {
            numberArray = numberArraySpare
            print(numberArray)
        }
        
    }
    func updateScore(score: Int) {
        currentScore = score
        
        if currentScore > UserDefaults.standard.integer(forKey: "bestScore") {
            UserDefaults.standard.set(currentScore, forKey: "bestScore")
        }
        currentScore = 0
    }
    
    static var shared = Number()

}

