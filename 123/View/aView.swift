////
////  View.swift
////  123
////
////  Created by Anh Duy on 3/15/19.
////  Copyright © 2019 Anh Duy. All rights reserved.
////
//
import UIKit

class aView: UIView {

    @IBOutlet weak var label: UILabel?
    @IBOutlet weak var button: UIButton?
    
    var delegateFromCell: HandleNumberValue?
    var delegateFromGameController : GameViewControllerDelegate?
    var firstTouched: Bool = false
    
    @IBAction func buttonTapped() {
        
        if Number.shared.pauseGame {
            
        }
        else {
            startGame()
        }
        
    }
    
    func startGame() {
        
        guard let cellNumber = delegateFromCell?.passNumber() else { return }
        
        if cellNumber == Number.shared.count {
            // RIGHT
            
            delegateFromGameController?.passNumberToScoreView(score: Number.shared.count)
            let number = Number.shared.numberArray.removeFirst()
            delegateFromCell?.setNumber(number: number)
            delegateFromGameController?.resetCircle()
            self.label?.text = String(number)
            Number.shared.count += 1
            Number.shared.changeNumberArray()
        }
        else {
            // WRONG
            
            Number.shared.pauseGame = true
            endGame()
        }
        
    }
    
    func endGame() {
        print("Your score is \(Number.shared.count - 1)")
        Number.shared.updateScore(score: Number.shared.count - 1)
        Number.shared.configureNumberArray()
        UIView.animate(withDuration: 0.6, animations: {
            self.shake(duration: 0.5)
            self.backgroundColor = UIColor.red.withAlphaComponent(0.9)
        }) { (true) in
            self.delegateFromGameController?.presentPlayAgainView()
            self.backgroundColor = UIColor.blue.withAlphaComponent(0.6)
        }
        
    }
    func resetGame() {
        Number.shared.resetNumberArray()
        delegateFromGameController?.passNumberToScoreView(score: 0)
        delegateFromGameController?.reloadCollectionView()
    }
}
