//
//  LeadersBoardTableViewCell.swift
//  123
//
//  Created by Anh Duy on 3/26/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import UIKit

class LeadersBoardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
