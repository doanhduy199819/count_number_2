//
//  playAgianView.swift
//  123
//
//  Created by Anh Duy on 3/28/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import UIKit

class PlayAgianView: UIView {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var playAgainButton: UIButton!
    @IBOutlet weak var continueButton : UIButton!
    
    var delegateFromGameControler: GameViewControllerDelegate?
    
    func setUp() {
        
        label.text = "Your score is \(Number.shared.count - 1)"
    
        playAgainButton.layer.cornerRadius = 10
        continueButton.layer.cornerRadius = 10
    }
    
    @IBAction func playAgain() {
        
        self.isHidden = false
        Number.shared.updateScore(score: Number.shared.count - 1)
        Number.shared.resetNumberArray()
        
        delegateFromGameControler?.passNumberToScoreView(score: 0)
        delegateFromGameControler?.reloadCollectionView()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.frame.origin.x -= 600
            self.alpha = 0
        }) { (true) in
            
            self.isHidden = true
            self.frame.origin.x += 600
            
            self.delegateFromGameControler?.setPauseGame(bool: false)
            self.delegateFromGameControler?.resetCircle()
            
        }
    }

    @IBAction func continueGame() {
        
        self.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
            self.frame.origin.x += 600
        }) { (true) in
            self.delegateFromGameControler?.resetCircle()
            self.isHidden = true
            self.frame.origin.x -= 600
            self.alpha = 0
            self.delegateFromGameControler?.setPauseGame(bool: false)
            
        }
    }
}
