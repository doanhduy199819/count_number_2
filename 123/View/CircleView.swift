//
//  CircleView.swift
//  123
//
//  Created by Anh Duy on 3/24/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import UIKit

class CircleView: UIView {

  
    var delegateFromGameController: GameViewControllerDelegate?
    
    var timeLeftShapeLayer = CAShapeLayer()
    let bgShapeLayer = CAShapeLayer()
    
    var timeLeft: TimeInterval = 2
    var endTime: Date?
    
    var timeLabel = UILabel()
    var timer = Timer()
    // here you create your basic animation object to animate the strokeEnd
    
    let strokeIt = CABasicAnimation(keyPath: "strokeEnd")
    
    func updateTime() {
        if timeLeft > 0 {
            timeLeft = endTime?.timeIntervalSinceNow ?? 0
            timeLabel.text = timeLeft.time
        } else {
            timeLabel.text = "00:00"
            timer.invalidate()
            if !(delegateFromGameController?.passPauseGame() ?? false) {
                delegateFromGameController?.presentPlayAgainView()
            }
        }
    }
    
    func addTimeLabel() {
        timeLabel = UILabel()
        timeLabel.frame = self.frame
        timeLabel.textAlignment = .center
        timeLabel.center = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        timeLabel.text = timeLeft.time
        timeLabel.textColor = UIColor.white
        self.addSubview(timeLabel)
    }
    
    func drawBgShape() {

        bgShapeLayer.path = UIBezierPath(arcCenter: CGPoint(x: self.bounds.midX, y: self.bounds.midY), radius:
            self.bounds.width/2 - 10, startAngle: -90.degreesToRadians, endAngle: 270.degreesToRadians, clockwise: true).cgPath
        bgShapeLayer.strokeColor = UIColor.white.cgColor
        bgShapeLayer.fillColor = UIColor.clear.cgColor
        bgShapeLayer.lineWidth = 10
        bgShapeLayer.frame = self.bounds

        self.layer.addSublayer(bgShapeLayer)

    }
    func drawTimeLeftShape() {
        
        timeLeftShapeLayer.path = UIBezierPath(arcCenter: CGPoint(x: self.bounds.midX, y: self.bounds.midY), radius:
            self.bounds.width/2 - 10, startAngle: -90.degreesToRadians, endAngle: 270.degreesToRadians, clockwise: true).cgPath
        timeLeftShapeLayer.strokeColor = UIColor.red.withAlphaComponent(0.6).cgColor
        timeLeftShapeLayer.fillColor = UIColor.clear.cgColor
        timeLeftShapeLayer.lineWidth = 10
        timeLeftShapeLayer.frame = self.bounds
        self.layer.addSublayer(timeLeftShapeLayer)

    }
    
    func setUpStructIT() {
        strokeIt.fromValue = 0
        strokeIt.toValue = 1
        strokeIt.duration = timeLeft
        timeLeftShapeLayer.add(strokeIt, forKey: nil)
       
    }
    func setUpStructIT(times: Int) {
        strokeIt.fromValue = 0
        strokeIt.toValue = 1
        strokeIt.duration = 1
        strokeIt.repeatCount = Float(times)
        timeLeftShapeLayer.add(strokeIt, forKey: nil)
        
    }
    func circleLoop(times : Int) {
        drawBgShape()
        drawTimeLeftShape()
        setUpStructIT(times: times)
        timeLeftShapeLayer.add(strokeIt, forKey: nil)
    }
    
    
}

extension TimeInterval {
    var time: String {
        return String(format:"%02d:%02d", Int(self/60),  Int(ceil(truncatingRemainder(dividingBy: 60))) )
    }
}
extension Int {
    var degreesToRadians : CGFloat {
        return CGFloat(self) * .pi / 180
    }
}


