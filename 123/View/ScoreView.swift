//
//  ScoreView.swift
//  123
//
//  Created by Anh Duy on 3/23/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//
import Firebase
import UIKit

class ScoreView: UIView {
    
    @IBOutlet weak var currentScore: UILabel?
    @IBOutlet weak var bestScore: UILabel?
    
    func setUpLabels() {
        
        self.bestScore?.adjustsFontSizeToFitWidth = true
        self.bestScore?.text = "Best Score: \(UserDefaults.standard.integer(forKey: "bestScore"))"
        self.bestScore?.translatesAutoresizingMaskIntoConstraints = false
        self.bestScore?.topAnchor.constraint(equalTo: self.topAnchor, constant: 40).isActive = true
        self.bestScore?.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 30).isActive = true
        
        self.currentScore?.adjustsFontSizeToFitWidth = true
        self.currentScore?.adjustsFontForContentSizeCategory = true
    }
}

extension UIColor {
    
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
    static var randomDark: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
