//
//  CollectionViewCell.swift
//  123
//
//  Created by Anh Duy on 3/15/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import UIKit
import SpriteKit
import GameKit

var score = 0

class CollectionViewCell: UICollectionViewCell {
    

    @IBOutlet weak var label: UILabel?
    @IBOutlet weak var button: UIButton?

    var value: Int?
    var delegateFromGameController : GameViewControllerDelegate?
    var firstTouched: Bool = false

    @IBAction func buttonTapped() {

        if delegateFromGameController?.passPauseGame() ?? true {

        }
        else {
            startGame()
        }

    }

    func startGame() {

        guard let cellNumber = value else { return }


        if cellNumber == Number.shared.count {
            // RIGHT

            delegateFromGameController?.passNumberToScoreView(score: cellNumber)
            delegateFromGameController?.changeScoreViewBGColor(color: self.backgroundColor!)
            let number = Number.shared.numberArray.removeFirst()
            value = number
            delegateFromGameController?.resetCircle()
            self.label?.text = String(number)
            Number.shared.count += 1
            Number.shared.changeNumberArray()
            self.backgroundColor = UIColor.random
        }
        else {
            // WRONG

            stopGame()
        }

    }

    func stopGame() {
        
        print("Your score is \(Number.shared.count - 1)")
        
        UIView.animate(withDuration: 0.6, animations: {
            self.shake(duration: 0.6)
            self.backgroundColor = UIColor.red.withAlphaComponent(0.9)
        }) { (true) in

            self.delegateFromGameController?.presentPlayAgainView()
            self.backgroundColor = UIColor.blue.withAlphaComponent(0.6)

        }
    }
}

