//
//  Protocol.swift
//  123
//
//  Created by Anh Duy on 3/16/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import Foundation
import UIKit

protocol HandleNumberValue {
    func passNumber() -> Int
    func setNumber(number: Int)
}

protocol GameViewControllerDelegate {
    
    func passNumberToScoreView(score: Int)
    func reloadCollectionView()
    func popToMenu()
    func presentPlayAgainView()
    func setAllBlue()
    func resetCircle()
    func changeScoreViewBGColor(color: UIColor)
    func setPauseGame(bool: Bool)
    func passPauseGame() -> Bool
}

protocol MainControllerDelegate {
    func pushToGameController()
}
protocol WorldLeadersboardDelegate {
    func passScore()
}
protocol MenuControllerDelegate {
    func updateBestScore()
}

extension Notification.Name {
    static let didReceiveData = Notification.Name("didReceiveData")
    static let didChange = Notification.Name("didChange")
    static let didChangeBestScore = Notification.Name("didChangeBestScore")
}
