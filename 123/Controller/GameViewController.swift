//
//  MainViewController.swift
//  123
//
//  Created by Anh Duy on 3/15/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import UIKit
import Firebase
//let names: [String] = ["Ball", "Dave", "John"]
//let leadersBoard: [String : Int] = ["Ball" : 100, "Dave" : 80, "John" : 20 ]

class GameViewController: UIViewController {
    
    @IBOutlet weak var collectionView: CollectionView?
    @IBOutlet weak var scoreView: ScoreView?
    @IBOutlet weak var backButton: UIButton?
    @IBOutlet weak var playAgainView: PlayAgianView!
    @IBOutlet weak var circleView: CircleView?
    
    var pauseGame = false
    var endView: UIView!
    var delegateFromMenuController: MenuControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pauseGame = false
        setUpCollectionView()
        setUpScoreView()
        setUpPlayAgainView()
        setUpCirCleView()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Number.shared.resetNumberArray()
        print(pauseGame)
    }
    
    func setUpPlayAgainView() {
        
        playAgainView.setUp()
        playAgainView.isHidden = true
        playAgainView.alpha = 0
        playAgainView.center = view.center
        playAgainView.delegateFromGameControler = self
        
    }
    
    func setUpCirCleView() {

        circleView?.frame.size.width = view.frame.width/4
        circleView?.frame.size.height = view.frame.width/4
        
        circleView?.delegateFromGameController = self
        
        circleView?.drawBgShape()
        circleView?.drawTimeLeftShape()
        circleView?.addTimeLabel()
        circleView?.setUpStructIT()
        circleView?.endTime = Date().addingTimeInterval((circleView?.timeLeft)!)
        circleView?.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        circleView?.updateTime()
    }
    
    func setUpCollectionView() {
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
    }
    
    func setUpScoreView() {
        scoreView?.setUpLabels()
    }
    
    @IBAction func popToRoot() {
        navigationController?.popViewController(animated: true)
    }
    
}

extension GameViewController : UICollectionViewDataSource, UICollectionViewDelegate  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as? CollectionViewCell

        cell?.label?.textColor = UIColor.white
        cell?.clipsToBounds = true
        
        cell?.value = Number.shared.numberArray.removeFirst()
        Number.shared.changeNumberArray()

        let title = String(cell?.value ?? 0)
        cell?.label?.adjustsFontSizeToFitWidth = true
        cell?.label?.text = title
        
        cell?.layer.cornerRadius = 10
        cell?.backgroundColor = UIColor.random
        cell?.delegateFromGameController = self
        
        return cell ?? CollectionViewCell()
    }
}

extension GameViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.width - 20) / 4 - 20
        let size = CGSize(width: width, height: width)
        return size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        return inset
    }
    
}
extension GameViewController : GameViewControllerDelegate {
    
    func passNumberToScoreView(score: Int) {
        scoreView?.currentScore?.text = String(score)
        
        if score > UserDefaults.standard.integer(forKey: "bestScore") {
            UIView.animate(withDuration: 0.2) {
                self.scoreView?.bestScore?.textColor = UIColor.green
                UserDefaults.standard.set(score, forKey: "bestScore")
                NotificationCenter.default.post(name: .didChangeBestScore, object: nil)
            }
            scoreView?.bestScore?.text = "Best Score: \(score)"
        }
    }
    
    
    func reloadCollectionView() {
        collectionView?.reloadData()
    }
    
    func popToMenu() {
        navigationController?.popViewController(animated: true)
    }
    func presentPlayAgainView() {
        self.playAgainView.label.text = "Your score is \(self.scoreView?.currentScore?.text ?? "" )"
        self.playAgainView.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
            self.playAgainView.alpha = 1
        }) { (false) in
            self.pauseGame = true
            self.scoreView?.bestScore?.textColor = UIColor.white
            self.delegateFromMenuController?.updateBestScore()
            NotificationCenter.default.post(name: .didChange, object: nil)
        }
        
    }
    func setAllBlue() {
        
    }
    func resetCircle() {
        circleView?.timeLeft = 2
        circleView?.drawBgShape()
        circleView?.drawTimeLeftShape()
        circleView?.setUpStructIT()
        circleView?.endTime = Date().addingTimeInterval((circleView?.timeLeft)!)
        circleView?.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    func changeScoreViewBGColor(color: UIColor) {
        scoreView?.backgroundColor = color
    }
    func setPauseGame(bool: Bool) {
        pauseGame = bool
    }
    func passPauseGame() -> Bool {
        return pauseGame
    }
}
