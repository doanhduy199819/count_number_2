//
//  LeadersBoardViewController.swift
//  123
//
//  Created by Anh Duy on 3/26/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//
import Foundation
import UIKit
import Firebase
import FirebaseAuth

class WorldLeadersBoardViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var signOutButton: UIButton!
    
    var ref: DatabaseReference?
    var names: [String]?
    var scores: [Int]?
    var delegateFromMenuController: MenuControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView.layer.cornerRadius = 20
        backButton.layer.cornerRadius = 10
        signOutButton.layer.cornerRadius = 10

        ref = Database.database().reference()
        
        tableView.reloadData()
    }
    
    @IBAction func popBackToMenu() {
        navigationController?.popToRootViewController(animated: true)
    }
  
    @IBAction func signOut () {
        try! Auth.auth().signOut()
        UserDefaults.standard.set(0, forKey: "bestScore")
        NotificationCenter.default.post(name: .didChangeBestScore, object: nil)
        delegateFromMenuController?.updateBestScore()
        popBackToMenu()
    }
   

}
extension WorldLeadersBoardViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 || section == 1 {
            return 1
        }

        return names?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "leadersBoard", for: indexPath)
            cell.textLabel?.text = "Player Name"
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17)
            cell.detailTextLabel?.text = "Best Score"
            cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 17)
            return cell
        }
            
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "leadersBoard", for: indexPath)
            cell.textLabel?.text = "You"
            cell.textLabel?.textColor = UIColor.blue
            cell.detailTextLabel?.text = String(UserDefaults.standard.integer(forKey: "bestScore"))
            cell.detailTextLabel?.textColor = UIColor.blue
            return cell
        }
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "leadersBoard", for: indexPath)
        
        cell.textLabel?.text = names?[indexPath.row]
        cell.detailTextLabel?.text = String(scores?[indexPath.row] ?? 0)
        return cell
    }
    
    
}
