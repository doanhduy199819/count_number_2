//
//  LeadersBoardViewController.swift
//  123
//
//  Created by Anh Duy on 3/26/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//
import Foundation
import UIKit
import FirebaseAuth

class WorldLeadersBoardViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var backButton: UIButton!
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView?.delegate = self
        tableView?.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func popBackToMenu() {
        navigationController?.popToRootViewController(animated: true)
    }
  
    func setUpTableView() {
        
    }
    

}
extension WorldLeadersBoardViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }

        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "leadersBoard", for: indexPath)
            cell.textLabel?.text = "Player Name"
            cell.detailTextLabel?.text = "Best Score"
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "leadersBoard", for: indexPath)
        cell.textLabel?.text = names[indexPath.row]
        cell.detailTextLabel?.text = String(leadersBoard["\(indexPath.row)"] ?? 0)
        return cell
    }
    
    
}
