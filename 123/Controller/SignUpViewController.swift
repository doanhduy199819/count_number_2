//
//  SignUpViewController.swift
//  123
//
//  Created by Anh Duy on 3/27/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignUpViewController: UIViewController {

    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var checkNameButton: UIButton!
    @IBOutlet weak var backButton: UIButton!

    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var replyPWTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var replyPasswordLabel: UILabel!
    @IBOutlet weak var gameLogo: UILabel!
    
    var alert: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        signUpButton.layer.cornerRadius = 20
        
        signUpButton.titleLabel?.adjustsFontSizeToFitWidth = true
        checkNameButton.titleLabel?.adjustsFontSizeToFitWidth = true
        backButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        emailLabel.adjustsFontSizeToFitWidth = true
        nameLabel.adjustsFontSizeToFitWidth = true
        passwordLabel.adjustsFontSizeToFitWidth = true
        replyPasswordLabel.adjustsFontSizeToFitWidth = true
        gameLogo.adjustsFontSizeToFitWidth = true
        
        nameTextField.delegate = self
        passwordTextField.delegate = self
        replyPWTextField.delegate = self
        emailTextField.delegate = self
        
        alert = UIAlertController(title: "Alert", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert?.addAction(action)

    }
    
    @IBAction func signUp() {
        guard let name = nameTextField.text else {
            print("ID is empty")
            nameTextField.placeholder = "Invalid"
            nameTextField.textColor = UIColor.red
            return
        }
        guard let password = passwordTextField.text else {
            print("Password is empty")
            passwordTextField.placeholder = "Invalid"
            passwordTextField.textColor = UIColor.red
            return
        }
        guard let replyPW = replyPWTextField.text else {
            print("Reply password is empty")
            replyPWTextField.placeholder = "Invalid"
            replyPWTextField.textColor = UIColor.red
            return
        }
        guard let email = emailTextField.text else {
            print("Email is empty")
            emailTextField.placeholder = "Invalid"
            emailTextField.textColor = UIColor.red
            return
        }
        if replyPW != password {
            print("Password did not match !!")
            replyPWTextField.placeholder = "Invalid"
            replyPWTextField.textColor = UIColor.red
            return
        }
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            // ...
            print(authResult ?? "")
            guard let authResult = authResult else {
                
                if let error = error {
                    self.alert?.message = error.localizedDescription
                    self.present(self.alert!, animated: true, completion: nil)
                }
                return
            }
            self.alert?.message = "Sign up successful"
            
            let request = authResult.user.createProfileChangeRequest()
            request.displayName = name
            request.commitChanges(completion: nil)
            
            let pushAction = UIAlertAction(title: "Sign In", style: .default, handler: { (action) in
                
                let signIn = self.storyboard?.instantiateViewController(withIdentifier: "signin") as! SignInViewController
                self.navigationController?.pushViewController(signIn, animated: true)
                
            })
            self.alert?.addAction(pushAction)
            self.present(self.alert!, animated: true, completion: nil)
                        
        }
    
    }
    
    @IBAction func popToRoot() {
        navigationController?.popViewController(animated: true)
    }
}
extension SignUpViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
