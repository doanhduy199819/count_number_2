//
//  MainViewController.swift
//  123
//
//  Created by Anh Duy on 3/23/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    var naviController : UINavigationController?
    var menuController: MenuViewController?
    var gameController: GameViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
        
        // Do any additional setup after loading the view.
        
    }
    
    func initialize() {
        menuController = MenuViewController()
        menuController?.delegateFromMainController = self
        naviController = UINavigationController(rootViewController: menuController!)
        naviController?.didMove(toParent: self)
        gameController = GameViewController()
        menuController?.delegateFromGameController = gameController
    }
   

}
extension MainViewController: MainControllerDelegate {
    func pushToGameController() {
        self.present(gameController!, animated: true, completion: nil)
    }
    
}
