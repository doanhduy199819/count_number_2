//
//  SignInViewController.swift
//  123
//
//  Created by Anh Duy on 3/27/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class SignInViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var gameLogo: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    
    var alert : UIAlertController?
    var signInState: AuthStateDidChangeListenerHandle?
    var ref : DatabaseReference?
    var delegateFromWorldLeadersBoard: WorldLeadersboardDelegate?
    var delegateFromMenuController: MenuControllerDelegate?
//    var names: [String]?
//    var scores: [Int]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        signInButton.layer.cornerRadius = 20
        signInButton.titleLabel?.adjustsFontSizeToFitWidth = true
        gameLogo.adjustsFontSizeToFitWidth = true
        passwordLabel.adjustsFontSizeToFitWidth = true
        emailLabel.adjustsFontSizeToFitWidth = true
        alert = UIAlertController(title: "Alert", message: "message", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert?.addAction(action)
        ref = Database.database().reference()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Listen for authentication state
        signInState = Auth.auth().addStateDidChangeListener { (auth, user) in
            // ...
            print("State change !!!")
        }
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        Auth.auth().removeStateDidChangeListener(signInState!)
    }
    
    @IBAction func logIn() {
        
        guard let email = emailTextField.text else {
            print("Email is empty")
            emailTextField.placeholder = "Invalid"
            emailTextField.textColor = UIColor.red
            return
        }
        guard let password = passwordTextField.text else {
            print("Password is empty")
            passwordTextField.placeholder = "Invalid"
            passwordTextField.textColor = UIColor.red
            return
        }

        Auth.auth().signIn(withEmail: email, password: password) { (Data, Error) in
            
            // SIGN IN FAILED
            
            guard let data = Data else {
                if let error = Error {
                    self.alert?.message = error.localizedDescription
                    self.present(self.alert!, animated: true, completion: nil)
                }
                return
            }
            
            // SIGN IN SUCCESS
            
            self.alert?.message = "Sign in successful"
            
            let uid = data.user.uid
            let namee = data.user.displayName ?? "Guest #\(uid.suffix(5))"
            let query = self.ref?.child("players").child(uid)

            self.ref?.child("players").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in

                let value = snapshot.value as? NSDictionary
                guard let name = value?["name"] as? String, let score = value?["score"] as? Int else {
                    let differentName = namee
                    query?.setValue(["name" : differentName, "score": UserDefaults.standard.integer(forKey: "bestScore")])
                    UserDefaults.standard.set(differentName, forKey: "signInName")
                    return
                }
                
                UserDefaults.standard.set(name, forKey: "signInName")
                print("Set userdefaultName")
                UserDefaults.standard.set(score, forKey: "bestScore")
                NotificationCenter.default.post(name: .didChangeBestScore, object: nil)
                print("Set userdefaultScore")
                self.delegateFromMenuController?.updateBestScore()
                
            })
            
            let pushAction = UIAlertAction(title: "LeadersBoard", style: .default, handler: { (action) in
                let leaderboard = self.storyboard?.instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
                self.navigationController?.pushViewController(leaderboard, animated: true)
            })
            self.alert?.addAction(pushAction)
            self.present(self.alert!, animated: true, completion: nil)
        }
    }
    
    @IBAction func popToRoot() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUp() {
        navigationController?.pushViewController(storyboard?.instantiateViewController(withIdentifier: "signup") as! SignUpViewController, animated: true)
    }
   
}
extension SignInViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
