//
//  MenuViewController.swift
//  123
//
//  Created by Anh Duy on 3/23/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//
import Firebase
import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var gameLogo: UILabel?
    @IBOutlet weak var playButton: UIButton?
    @IBOutlet weak var bestScore: UILabel?
    @IBOutlet weak var signInButton: UIButton?
    @IBOutlet weak var signUpButton: UIButton?
    @IBOutlet weak var leadersBoardButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        bestScore?.text = "Best Score: \(UserDefaults.standard.integer(forKey: "bestScore"))"
        bestScore?.adjustsFontSizeToFitWidth = true
        gameLogo?.adjustsFontSizeToFitWidth = true
        playButton?.clipsToBounds = true
        playButton?.layer.cornerRadius = 10
        navigationController?.isNavigationBarHidden = true
        leadersBoardButton.clipsToBounds = true
        leadersBoardButton.layer.cornerRadius = 10
        leadersBoardButton.titleLabel?.adjustsFontSizeToFitWidth = true
        signInButton?.titleLabel?.adjustsFontSizeToFitWidth = true
        signUpButton?.titleLabel?.adjustsFontSizeToFitWidth = true
        playButton?.titleLabel?.adjustsFontSizeToFitWidth = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAnotherBestScore), name: .didChangeBestScore, object: nil)
    }
    
    @objc func updateAnotherBestScore() {
        bestScore?.text = "Best Score: \(UserDefaults.standard.integer(forKey: "bestScore"))"
    }
    
    
    @IBAction func pushToGameController() {
        navigationController?.pushViewController(storyboard?.instantiateViewController(withIdentifier: "game") as! GameViewController, animated: true)
        
    }
    
    @IBAction func pushToLeadersBoard() {
        
        if Auth.auth().currentUser != nil {
              navigationController?.pushViewController(storyboard?.instantiateViewController(withIdentifier: "tabbar") as! UITabBarController, animated: true)
        }
        else {
            navigationController?.pushViewController(storyboard?.instantiateViewController(withIdentifier: "signin") as! SignInViewController, animated: true)
        }
        
     
    }
    
    @IBAction func pushToSignIn() {
        navigationController?.pushViewController(storyboard?.instantiateViewController(withIdentifier: "signin") as! SignInViewController, animated: true)
    }
    
    @IBAction func pushToSignUp() {
        navigationController?.pushViewController(storyboard?.instantiateViewController(withIdentifier: "signup") as! SignUpViewController, animated: true)
    }
    
    
}
extension MenuViewController : MenuControllerDelegate {
    func updateBestScore() {
        bestScore?.text = "Best Score: \(UserDefaults.standard.integer(forKey: "bestScore"))"
        reloadInputViews()
        bestScore?.reloadInputViews()
    }
}

