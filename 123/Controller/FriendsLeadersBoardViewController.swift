//
//  FriendsLeadersBoardViewController.swift
//  123
//
//  Created by Anh Duy on 3/27/19.
//  Copyright © 2019 Anh Duy. All rights reserved.
//

import UIKit
import Firebase

class FriendsLeadersBoardViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var signOutButton: UIButton!
    
    var ref: DatabaseReference?
    var names : [String]? = []
    var scores : [Int]? = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.cornerRadius = 20
        backButton.layer.cornerRadius = 10
        signOutButton.layer.cornerRadius = 10

        ref = Database.database().reference()

        let query = self.ref?.child("players")
        
        query?.queryOrdered(byChild: "score").observe(.childAdded, with: { (snapshot) in
            let value = snapshot.value as? [String : Any]
            let name = value?["name"] as? String
            self.names?.append(name ?? "Guest")
            print(name)
            print(self.names)
            let score = value?["score"] as? Int
            self.scores?.append(score ?? 0)
            self.tableView.reloadData()
        }, withCancel: nil)
    }

    @IBAction func popBackToMenu() {
        navigationController?.popToRootViewController(animated: true)
    }
    
}
extension FriendsLeadersBoardViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        return names?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "leadersBoard", for: indexPath)
            cell.textLabel?.text = "Player Name"
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 17)
            cell.detailTextLabel?.text = "Best Score"
            cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 17)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "leadersBoard", for: indexPath)
        if UserDefaults.standard.string(forKey: "signInName") == names?[indexPath.row] {
            cell.textLabel?.textColor = UIColor.blue
            cell.detailTextLabel?.textColor = UIColor.blue
        }
        cell.textLabel?.text = names?[indexPath.row]
        cell.detailTextLabel?.text = String(scores?[indexPath.row] ?? 0)
        return cell
        
    }
}
